import withRoot from './modules/withRoot';
// --- Post bootstrap -----
import React from 'react';


import {AppTopBar,AppFooter,AppHeader,FrameworksCategories,UserContact,HowItWorks,Cycle,UserContactLayout} from "./modules/views";

function Index() {
  return (
    <React.Fragment>
      <AppTopBar />
      <AppHeader />
      <Cycle />
      <FrameworksCategories />
      <HowItWorks />
      <UserContact />
      <UserContactLayout />
      <AppFooter />
    </React.Fragment>
  );
}

export default withRoot(Index);
