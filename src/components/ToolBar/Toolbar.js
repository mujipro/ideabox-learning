import { withStyles } from '@material-ui/core/styles';
import ToolbarComponent from '@material-ui/core/Toolbar';

export const stylesToolbar = theme => ({
  root: {
    height: 64,
    [theme.breakpoints.up('sm')]: {
      height: 70,
    },
  },
});

export const Toolbar =  withStyles(stylesToolbar)(ToolbarComponent);
