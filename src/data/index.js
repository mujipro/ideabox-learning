export const frameworks = [
    {
        url: require('../assets/images/frameworks/angular.png'),
        title: 'ANGULAR',
        width: '25%',
        links: [
            {   id:1,
                author: 'Mosh Hamedani',
                url:'https://www.udemy.com/course/angular-crash-course/learn/lecture/7465694#overview'
            },
            {
                id:2,
                author: 'Ray Lillabos',
                url:'https://www.linkedin.com/learning/learning-angular/working-with-two-way-data-binding'
            },
            {
                id:3,
                author: 'Free Code Camp',
                url:'https://www.freecodecamp.org/news/tag/angular/'
            },
        ]
    },
    {
        url: require('../assets/images/frameworks/react.png'),
        title: 'REACT NATIVE',
        width: '25%',
        links: [
            {
                id:1,
                author: 'Steven Grider',
                url:'https://www.udemy.com/course/the-complete-react-native-and-redux-course/learn/lecture/5744914?start=0#overview'
            },
            {
                id:2,
                author: 'Free Code Camp',
                url:'https://www.freecodecamp.org/news/tag/react-native/'
            },
            {
                id:3,
                author: 'Medium',
                url:'https://medium.com/search?q=react%20native'
            },
            {
                id:4,
                author: 'React Redux',
                url: 'https://react-redux.js.org/'
            },
            {
                id:5,
                author: 'Mujtaba',
                url: 'git clone https://mujtabaayub@bitbucket.org/ideaboxdev/react-native-boilerplate-v1.git'
            }
        ]
    },
    {
        url: require('../assets/images/frameworks/reactJs.png'),
        title: 'REACT JS',
        width: '25%',
        links: [
            {   id:1,
                author: 'Medium',
                url:'https://medium.com/search?q=react%20js'},
            {   id:2,
                author: 'Material UI',
                url:'https://material-ui.com/'
            },
            {   id:3,
                author: 'Mujtaba',
                url:'git clone https://mujtabaayub@bitbucket.org/ideaboxdev/reactjs-boilerplate-v1.git'
            },
        ]
    },
    {
        url: require('../assets/images/frameworks/node.jpg'),
        title: 'Node',
        width: '25%',
        links: [
            {
                id:1,
                author: 'Free Code Camp',
                url:'https://www.freecodecamp.org/news/tag/node/'},
            {
                id:2,
                author: 'Medium',
                url:'https://medium.com/search?q=node%20js'
            },
            {  id:3,
                author: 'Mujtaba',
                url:'git clone https://mujtabaayub@bitbucket.org/ideaboxdev/nodejs-boilerplate-v1.git'
            },
        ]
    },
    {
        url: require('../assets/images/frameworks/C#.jpg'),
        title: 'C#',
        width: '33.33%',
        links: [
            {
                id:1,
                author: 'Ideabox',
                url:'C#.com'},
            {
                id:2,
                author: 'Ideabox',
                url:'C#1.com'
            },
            {
                id:3,
                author: 'Ideabox',
                url:'C#2.com'
            },
        ]
    },
    {
        url: require('../assets/images/frameworks/Php.png'),
        title: 'Php',
        width: '33.33%',
        links: [
            {
                id:1,
                author: 'Ideabox',
                url:'php.com'},
            {
                id:2,
                author: 'Ideabox',
                url:'php1.com'
            },
            {   id:3,
                author: 'Ideabox',
                url:'php2.com'
            },
        ]
    },
    {
        url: require('../assets/images/frameworks/Jest.png'),
        title: 'Jest',
        width: '33.33%',
        links: [
            {   id:1,
                author: 'Free Code Camp',
                url:'https://www.freecodecamp.org/news/tag/jest/'
            },
            {
                id:2,
                author: 'Emmanuel Henri',
                url:'https://www.linkedin.com/learning/react-testing-and-debugging/welcome',
            }
        ]
    },
    {
        url: require('../assets/images/frameworks/mysql.gif'),
        title: 'MY SQL',
        width: '33.33%',
        links: [
            {
                id:1,
                author: 'Ideabox',
                url:'mysql.com'
            },
            {
                id:2,
                author: 'Ideabox',
                url:'mysql1.com'
            },
            {
                id:3,
                author: 'Ideabox',
                url:'mysql2.com'
            },
        ]
    },
    {
        url: require('../assets/images/frameworks/wordPress.jpg'),
        title: 'Word Press',
        width: '33.33%',
        links: [
            {   id:1,
                author: 'Morten Rand-Hendriksen',
                url:'https://www.linkedin.com/learning/wordpress-4-essential-training/going-further-with-wordpress'},
            {   id:2,
                author: 'Patrick Rauland',
                url:'https://www.linkedin.com/learning/wordpress-seo'
            }
        ]
    },
    {
        url: require('../assets/images/frameworks/yii.png'),
        title: 'YII',
        width: '33.33%',
        links: [
            {
                id:1,
                author: 'Ideabox',
                url:'yii.com'
            },
            {
                id:2,
                author: 'Ideabox',
                url:'yii1.com'
            },
            {   id:3,
                author: 'Ideabox',
                url:'yii2.com'
            },
        ]
    },
];
