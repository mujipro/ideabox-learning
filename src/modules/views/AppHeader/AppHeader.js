import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {Typography} from '../../../components';
import {AppHeaderLayout} from '../AppHeaderLayout/AppHeaderLayout';

const backgroundImage = require("../../../assets/Ideabox.png");

const styles = theme => ({
  background: {
    backgroundImage: `url(${backgroundImage})`,
    backgroundColor: '#7fc7d9', // Average color of the background image.
    backgroundPosition: 'center',
  },
  button: {
    minWidth: 200,
  },
  h5: {
    marginBottom: theme.spacing(4),
    marginTop: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing(10),
    },
  },
  more: {
    marginTop: theme.spacing(2),
  },
});

function AppHeaderScreen(props) {
  const { classes } = props;

  return (
    <AppHeaderLayout backgroundClassName={classes.background}>
      {/* Increase the network loading priority of the background image. */}
      <img style={{ display: 'none' }} src={backgroundImage} alt="increase priority" />
      <Typography color="inherit" align="center" variant="h2" marked="center">
        IDEABOX LEARNING PORTAL
      </Typography>
      <Typography variant="body2" color="inherit" className={classes.more}>
        IMRPROVE YOUR EXPERIENCE BY MAKING USE OF THE BEST PRACTICES AND TECHNIQUES THAT IS USED WORLD WIDE
      </Typography>
    </AppHeaderLayout>
  );
}

AppHeaderScreen.propTypes = {
  classes: PropTypes.object.isRequired,
};

export const AppHeader =  withStyles(styles)(AppHeaderScreen);
