import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import {Button,Typography} from '../../../components';

import MenuBookIcon from '@material-ui/icons/MenuBook';
import SettingsEthernetIcon from '@material-ui/icons/SettingsEthernet';
import DeveloperModeIcon from '@material-ui/icons/DeveloperMode';

const styles = theme => ({
    root: {
        display: 'flex',
        backgroundColor: theme.palette.secondary.light,
        overflow: 'hidden',
    },
    container: {
        marginTop: theme.spacing(10),
        marginBottom: theme.spacing(15),
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    item: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(0, 5),
    },
    title: {
        marginBottom: theme.spacing(14),
    },
    number: {
        fontSize: 24,
        fontFamily: theme.typography.fontFamily,
        color: theme.palette.secondary.main,
        fontWeight: theme.typography.fontWeightMedium,
    },
    icon: {
        fontSize: 100,
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
    },
    curvyLines: {
        pointerEvents: 'none',
        position: 'absolute',
        top: -180,
        opacity: 0.7,
    },
    button: {
        marginTop: theme.spacing(8),
    },
});

function HowItWorksScreen(props) {
    const { classes } = props;

    return (
        <section className={classes.root}>
            <Container className={classes.container}>
                <img
                    src={require("../../../assets/themes/onepirate/productCurvyLines.png")}
                    className={classes.curvyLines}
                    alt="curvy lines"
                />
                <Typography variant="h4" marked="center" className={classes.title} component="h2">
                    How it works
                </Typography>
                <div>
                    <Grid container spacing={5}>
                        <Grid item xs={12} md={4}>
                            <div className={classes.item}>
                                <div className={classes.number}>1.</div>
                                <MenuBookIcon className={classes.icon} />
                                <Typography variant="h5" align="center">
                                    LEARN NEW METHODOLOGIES
                                </Typography>
                            </div>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <div className={classes.item}>
                                <div className={classes.number}>2.</div>
                                <SettingsEthernetIcon className={classes.icon} />
                                <Typography variant="h5" align="center">
                                    PRACTICE NEW METHODOLOGIES
                                </Typography>
                            </div>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <div className={classes.item}>
                                <div className={classes.number}>3.</div>
                                <DeveloperModeIcon  className={classes.icon} />
                                <Typography variant="h5" align="center">
                                    {'DEVELOP USING THESE NEW. '}
                                    {'METHODOLOGIES IN LIVE PROJECT.'}
                                </Typography>
                            </div>
                        </Grid>
                    </Grid>
                </div>
                <Button
                    color="secondary"
                    size="large"
                    variant="contained"
                    className={classes.button}
                    component="a"
                    href="/premium-themes/onepirate/sign-up/"
                >
                    Get started
                </Button>
            </Container>
        </section>
    );
}

HowItWorksScreen.propTypes = {
    classes: PropTypes.object.isRequired,
};

export const HowItWorks =  withStyles(styles)(HowItWorksScreen);
