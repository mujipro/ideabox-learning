import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ButtonBase from '@material-ui/core/ButtonBase';
import Container from '@material-ui/core/Container';
import {Typography} from '../../../components';



import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Grid from '@material-ui/core/Grid';

import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import {frameworks} from "../../../data";




const styles = theme => ({
  root: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(4),
  },
  images: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexWrap: 'wrap',
  },
  imageWrapper: {
    position: 'relative',
    display: 'block',
    padding: 0,
    borderRadius: 0,
    height: '40vh',
    [theme.breakpoints.down('sm')]: {
      width: '100% !important',
      height: 100,
    },
    '&:hover': {
      zIndex: 1,
    },
    '&:hover $imageBackdrop': {
      opacity: 0.15,
    },
    '&:hover $imageMarked': {
      opacity: 0,
    },
    '&:hover $imageTitle': {
      border: '4px solid currentColor',
    },
  },
  imageButton: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.common.white,
  },
  imageSrc: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: 'cover',
    backgroundPosition: 'center 40%',
  },
  imageBackdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    background: theme.palette.common.black,
    opacity: 0.5,
    transition: theme.transitions.create('opacity'),
  },
  imageTitle: {
    position: 'relative',
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px 14px`,
  },
  imageMarked: {
    height: 3,
    width: 18,
    background: theme.palette.common.white,
    position: 'absolute',
    bottom: -2,
    left: 'calc(50% - 9px)',
    transition: theme.transitions.create('opacity'),
  },
  appBar: {
    position: 'relative',
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)'
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
    color: 'inherit'
  }
});

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function FrameworkCategoriesScreen(props) {
  const { classes } = props;
  const [open, setOpen] = React.useState(false);
  const [frameWorkInfo, setframeWorkInfo] = React.useState([]);

  const handleClickOpen = (framework) => {
    setOpen(true);
    setframeWorkInfo([{url: framework.url, frameWorkTitle: framework.title,links: framework.links }]);
  };

  const handleClose = () => {
    setOpen(false);
  };




  return (
    <Container className={classes.root} component="section">
      <Typography variant="h4" marked="center" align="center" component="h2">
        For all tastes and all desires
      </Typography>
      <div className={classes.images}>
        {frameworks.map(framework => (
          <ButtonBase
            key={framework.title}
            className={classes.imageWrapper}
            style={{
              width: framework.width,
            }}
            onClick={() =>{
              handleClickOpen(framework)
            }}
          >
            <div
              className={classes.imageSrc}
              style={{
                backgroundImage: `url(${framework.url})`,
              }}
            />
            <div className={classes.imageBackdrop} />
            <div className={classes.imageButton}>
              <Typography
                component="h3"
                variant="h6"
                color="inherit"
                className={classes.imageTitle}
              >
                {framework.title}
                <div className={classes.imageMarked} />
              </Typography>
            </div>
          </ButtonBase>
        ))}
      </div>

      <div>
        <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                <CloseIcon />
              </IconButton>
              <Typography variant="h6" className={classes.title}>
                {frameWorkInfo[0] && frameWorkInfo[0]["frameWorkTitle"] ? frameWorkInfo[0]["frameWorkTitle"] : "No title" }
              </Typography>
              <Button autoFocus color="inherit" onClick={handleClose}>
                save
              </Button>
            </Toolbar>
          </AppBar>
          <List>
            {

              frameWorkInfo[0] && frameWorkInfo[0]["links"].length > 0 ?
                  frameWorkInfo[0]["links"].map(link => (


                      <Grid key={link.id} xs={12} item={true} md={12} >
                      <ListItem button>
                        <ListItemAvatar>
                          <Avatar style={{backgroundColor: '#FFF'}} >
                           <img alt='' style={{ display: 'block', width: '100%'}} src={frameWorkInfo[0]["url"]}/>
                          </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary={link.author} secondary={link.url} />
                      </ListItem>
                      <Divider/>
                      </Grid>

                  )) : null

            }

          </List>
        </Dialog>
      </div>

    </Container>
  );
}

FrameworkCategoriesScreen.propTypes = {
  classes: PropTypes.object.isRequired,
};

export const FrameworksCategories =  withStyles(styles)(FrameworkCategoriesScreen);
