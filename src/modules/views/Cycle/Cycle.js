import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import {Typography} from '../../../components';

import AddToQueueIcon from '@material-ui/icons/AddToQueue';
import CodeIcon from '@material-ui/icons/Code';
import BeenhereIcon from '@material-ui/icons/Beenhere';

const styles = theme => ({
    root: {
        display: 'flex',
        overflow: 'hidden',
        backgroundColor: theme.palette.secondary.light,
    },
    container: {
        marginTop: theme.spacing(15),
        marginBottom: theme.spacing(30),
        display: 'flex',
        position: 'relative',
    },
    item: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(0, 5),
    },
    image: {
        height: 55,
    },
    title: {
        marginTop: theme.spacing(5),
        marginBottom: theme.spacing(5),
    },
    curvyLines: {
        pointerEvents: 'none',
        position: 'absolute',
        top: -180,
    },
});

function CycleScreen(props) {
    const { classes } = props;

    return (
        <section className={classes.root}>
            <Container className={classes.container}>
                <img
                    src="/src/assets/themes/onepirate/productCurvyLines.png"
                    className={classes.curvyLines}
                    alt="curvy lines"
                />
                <Grid container spacing={5}>
                    <Grid item xs={12} md={4}>
                        <div className={classes.item}>
                            <AddToQueueIcon style={{fontSize:180}} />
                            <Typography variant="h6" className={classes.title}>
                                DESIGN
                            </Typography>
                            <Typography variant="h5">
                                {'Design using the best practices such as SCSS,CSS'}
                                {', and CSS-Grid with responsive and extensive design with reusable components.'}
                            </Typography>
                        </div>
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <div className={classes.item}>
                            <CodeIcon  style={{fontSize:180}} />
                            <Typography variant="h6" className={classes.title}>
                                DEVELOP
                            </Typography>
                            <Typography variant="h5">
                                {'Develop using the best libraries and Framework'}
                                {'Make use of the inbuilt features and methodologies.'}
                            </Typography>
                        </div>
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <div className={classes.item}>
                            <BeenhereIcon style={{fontSize:160}} />
                            <Typography variant="h6" className={classes.title}>
                                TEST
                            </Typography>
                            <Typography variant="h5">
                                {'Test driven development (TDD) approach or post '}
                                {'development test using jest or karma or any other libraries.'}
                            </Typography>
                        </div>
                    </Grid>
                </Grid>
            </Container>
        </section>
    );
}

CycleScreen.propTypes = {
    classes: PropTypes.object.isRequired,
};

export const Cycle =  withStyles(styles)(CycleScreen);
